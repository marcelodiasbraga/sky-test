//
//  CollectionViewCell.swift
//  SkyExercise
//
//  Created by Marcelo on 01/01/2018.
//

import UIKit

class CollectionViewCell : UICollectionViewCell {
    
    @IBOutlet var movieImage : UIImageView!
    
    func displayContent(image: UIImage) {
        movieImage.image = image
    }
    
}
