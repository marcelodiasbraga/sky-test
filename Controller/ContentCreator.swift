//
//  ContentCreator.swift
//  SkyExercise
//
//  Created by Marcelo Braga on 01/01/2018.
//

import Foundation
import UIKit

class ContentCreator  {
    var catalog = [[String:AnyObject]]()
    var image = UIImage()

    init() {
        
    }
    
    init(imageUrl : String) {
        self.image = getImageFromUrl(imageUrl)
    }
    

    
    
    init(url : String,delegate sender: ContentDelegate) {
        let url = URL(string: url)
        self.image = UIImage()
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            
            if error != nil {
                print(error!.localizedDescription)
                
                let jsonError = [["Error" : error!.localizedDescription]]
                
                sender.didFinishJsonLoad(
                    jsonError as Array<Dictionary<String, AnyObject>>
                )
                return
            }
            
            
            sender.didFinishJsonLoad(
                self.parseJson(
                    String(data: data!, encoding: .utf8)!
                )
            
            )
        }
        task.resume()
    }
    
    func getImageFromUrl (_ imageUrl : String ) -> UIImage {
        
        let url = URL(string: imageUrl)
        let data = try? Data(contentsOf: url!)
        if let imageData = data {
            let image = UIImage(data: imageData)
            return image!
        }
        return UIImage()
        
    }

    //Esse método foi construído apenas para testar a carga do catalog, através da função de teste unitário
    func getCatalogFromUrl (_ url : String ) -> Array<Dictionary<String, AnyObject>> {
        
        let url = URL(string: url)
        let data = try? Data(contentsOf: url!)
        
        if data != nil {
            return parseJson(
                String(data: data!, encoding: .utf8)!
            )
        }
        return [[String:AnyObject]]()
        
    }

    
    
    func parseJson(_ jSON : String) -> Array<Dictionary<String, AnyObject>>     {
        
        if let data = jSON.data(using: .utf8, allowLossyConversion: false) {
            let jsonReturn =  try! JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let movies = jsonReturn as? Array<Dictionary<String, AnyObject>> {
                return movies
            }
        }
        return Array<Dictionary<String, AnyObject>>()
    }

    
    
    
}

    
    

