//
//  ViewController.swift
//  SkyExercise
//
//  Created by Marcelo on 01/01/2018.
//  Copyright © 2018 Mobile Field. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource,  ContentDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    var movies = [[String:AnyObject]]()
    let alertWait = UIAlertController(title: "", message: "Por favor, aguarde...", preferredStyle: .alert)
    
    
    override func viewDidLoad() {
        startLife()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let row = indexPath.row
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) 

        if let coverImage = self.movies[row]["cover_url"] as? String {
            
            //Adding cover image programmatically
            let imageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: cell.frame.size.width, height: cell.frame.size.height - 30))
            imageView.image = ContentCreator(imageUrl: coverImage).image
            imageView.contentMode = UIViewContentMode.scaleAspectFit
            cell.contentView.addSubview(imageView)
            
        }
            
        if let title = self.movies[row]["title"] as? String {
                
                //Adding title label programmatically
                let titleLabel:UILabel = {
                    let label = UILabel(frame: CGRect(x:0.0, y: cell.frame.size.height - 30 , width: cell.frame.size.width , height: 30))
                    label.textAlignment = .center
                    label.lineBreakMode = .byClipping
                    label.numberOfLines = 0
                    label.textColor = UIColor.white
                    label.backgroundColor = UIColor.black
                    label.text = title
                    label.isHidden = false
                    label.font = label.font.withSize(10)
                    return label
                }()
                cell.contentView.addSubview(titleLabel)
        }
        
        return cell
        
    }

    func startLife() {
        DispatchQueue.main.async {
            self.present(self.alertWait, animated: true,completion: {
                ContentCreator(url: "https://sky-exercise.herokuapp.com/api/Movies", delegate: self)
            })
        }

    }
    
    func closeWait(witherror : Bool = false, newMessage message : String = "") {
        if(!witherror) {
            self.alertWait .dismiss(animated: true, completion: nil)
            return
        }
        self.alertWait .dismiss(animated: true, completion: {
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
                let okButton = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                alertController.addAction(okButton)
                self.present(alertController, animated: true, completion: nil)
        })
    }
    
    
    
    func didFinishJsonLoad(_ json: Array<Dictionary<String, AnyObject>>) {
        //Force execute on main thread
        DispatchQueue.main.async {
            
            if(json != nil && json.count == 0 ) {
                self.closeWait(witherror: true, newMessage: "Desculpe! Não foi possível carregar o catálogo.")
                return
            }
            
            
            if let errorReturn = json[0]["Error"] {
                self.closeWait(witherror: true,newMessage: "Não foi possível carregar o catálogo.")
            } else {
                self.movies = json
                self.closeWait()
                self.collectionView.reloadData()
            }
            
        }
    }

    
    
    
}

