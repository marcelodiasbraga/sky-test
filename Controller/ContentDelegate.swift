//
//  ContentDelegate.swift
//  SkyExercise
//
//  Created by Marcelo on 01/01/2018.
//  Copyright © 2018 Mobile Field. All rights reserved.
//

import Foundation

protocol ContentDelegate   {
    func didFinishJsonLoad(_ json : Array<Dictionary<String, AnyObject>>)
}
