//
//  SkyExerciseTests.swift
//  SkyExerciseTests
//
//  Created by Marcelo on 01/01/2018.
//  Copyright © 2018 Mobile Field. All rights reserved.
//

import XCTest
@testable import SkyExercise




class SkyExerciseTests: XCTestCase, ContentDelegate {
    
    
    var catalog = [[String:AnyObject]]()
    
    func didFinishJsonLoad(_ json: Array<Dictionary<String, AnyObject>>) {
        self.catalog = json

        
        
    }

    override func setUp() {
        super.setUp()

        let creator = ContentCreator()
        self.catalog = creator.getCatalogFromUrl("https://sky-exercise.herokuapp.com/api/Movies")

        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        self.catalog = [[String : AnyObject]]()
        
    }

    func testLoadCatalog() {
        
        let loaded:Bool = self.catalog.count > 0
        
        XCTAssertTrue(loaded)
    }
    
    
    func testLoadImages() {
        var count = 0
        if(self.catalog.count == 0 ) {
            count = -1
        }
        for movie in self.catalog {
            if let urlImage = movie["cover_url"] {
                let image = ContentCreator(imageUrl: urlImage as! String)
                count += 1
            }
        }
        XCTAssertEqual(self.catalog.count, count, "Imagens carregadas")
        
        
    }
    
    
    
}
